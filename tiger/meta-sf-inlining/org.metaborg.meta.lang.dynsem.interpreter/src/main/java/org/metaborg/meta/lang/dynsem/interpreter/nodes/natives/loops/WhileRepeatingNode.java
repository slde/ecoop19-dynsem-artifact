package org.metaborg.meta.lang.dynsem.interpreter.nodes.natives.loops;

import org.metaborg.meta.lang.dynsem.interpreter.nodes.DynSemNode;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.building.TermBuild;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.rules.RuleInvokeNode;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.rules.RuleInvokeNodeGen;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.rules.RuleResult;

import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.RepeatingNode;
import com.oracle.truffle.api.profiles.BranchProfile;
import com.oracle.truffle.api.profiles.LoopConditionProfile;
import com.oracle.truffle.api.source.SourceSection;

import mb.flowspec.runtime.interpreter.TypesGen;

public class WhileRepeatingNode extends DynSemNode implements RepeatingNode {

	private final FrameSlot componentsFrameSlot;
	private final FrameSlot resultFrameSlot;

	@Child private RuleInvokeNode conditionInvokeNode;
	@Child private RuleInvokeNode bodyInvokeNode;

	public WhileRepeatingNode(SourceSection source, TermBuild conditionBuildNode, TermBuild bodyBuildNode,
			FrameSlot componentsFrameSlot, FrameSlot resultFrameSlot) {
		super(source);
		this.conditionInvokeNode = RuleInvokeNodeGen.create(source, "", conditionBuildNode);
		this.bodyInvokeNode = RuleInvokeNodeGen.create(source, "", bodyBuildNode);
		this.componentsFrameSlot = componentsFrameSlot;
		this.resultFrameSlot = resultFrameSlot;
	}

	private final LoopConditionProfile conditionProfile = LoopConditionProfile.createCountingProfile();
	private final BranchProfile continueTaken = BranchProfile.create();
	private final BranchProfile breakTaken = BranchProfile.create();

	@Override
	public boolean executeRepeating(VirtualFrame frame) {
		// FIXME: for now we're ignoring updates to RW components

		// get the input components
		Object[] callArgs = (Object[]) frame.getValue(componentsFrameSlot);

		if (conditionProfile.profile(evaluateCondition(frame, callArgs))) {
			try {
				RuleResult bodyResult = this.bodyInvokeNode.executeGeneric(frame, callArgs);
				propagateRwComps(bodyResult.components, callArgs);
				frame.setObject(resultFrameSlot, bodyResult.result);
				return true;
			} catch (LoopContinueException cex) {
				continueTaken.enter();
				propagateRwComps(cex.getComponents(), callArgs);
				frame.setObject(resultFrameSlot, cex.getThrown());
				return true;
			} catch (LoopBreakException brex) {
				breakTaken.enter();
				propagateRwComps(brex.getComponents(), callArgs);
				frame.setObject(resultFrameSlot, brex.getThrown());
				return false;
			}
		} else {
			return false;
		}

	}

	private boolean evaluateCondition(VirtualFrame frame, Object[] callArgs) {
		RuleResult conditionResult = this.conditionInvokeNode.executeGeneric(frame, callArgs);
		propagateRwComps(conditionResult.components, callArgs);
		return TypesGen.asBoolean(conditionResult.result);
	}

	private void propagateRwComps(Object[] fromResult, Object[] toCallArgs) {
		int numrwcomps = fromResult.length;
		int callArgsoffset = toCallArgs.length - numrwcomps;
		for (int i = 0; i < numrwcomps; i++) {
			toCallArgs[callArgsoffset + i] = fromResult[i];
		}
	}
}
