#! /bin/bash

set -e
set -x

ROOTDIR="$(pwd)/../../"
pushd $ROOTDIR
source env.sh
popd

export JAVA_HOME="$GVM_HOME"

mkdir -p $ROOTDIR/results/java

for i in {0..29}
do
  nice -n -5 java List $WORKLOAD > $ROOTDIR/results/java/list_$i.out 2>&1 || echo ">>>> RUN CRASHED <<<<"
  nice -n -5 java Permute $WORKLOAD > $ROOTDIR/results/java/permute_$i.out 2>&1 || echo ">>>> RUN CRASHED <<<<"
  nice -n -5 java Queens $WORKLOAD > $ROOTDIR/results/java/queens2_$i.out 2>&1 || echo ">>>> RUN CRASHED <<<<"
  nice -n -5 java Sieve $WORKLOAD > $ROOTDIR/results/java/sieve_$i.out 2>&1 || echo ">>>> RUN CRASHED <<<<"
  nice -n -5 java Towers $WORKLOAD > $ROOTDIR/results/java/towers_$i.out 2>&1 || echo ">>>> RUN CRASHED <<<<"
  nice -n -5 java Bubblesort $WORKLOAD > $ROOTDIR/results/java/bubblesort_$i.out 2>&1 || echo ">>>> RUN CRASHED <<<<"
done
