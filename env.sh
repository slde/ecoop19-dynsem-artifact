#! /bin/bash

export MAVEN_OPTS="-server -Xms512m -Xmx1024m -Xss16m"

# export BENCHMARKS=$(pwd)
export M2="$(pwd)/m2-local"
export MAVEN_SETTINGS="$M2/settings.xml"
export MAVEN_REPO="$M2/repository"
export GVM_HOME="$(pwd)/graalvm/graalvm-ee-1.0.0-rc9"
export PATH="$GVM_HOME/bin:$PATH"
