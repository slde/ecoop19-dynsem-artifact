package org.metaborg.meta.lang.dynsem.interpreter.nabl2.f.arrays;

import org.metaborg.meta.lang.dynsem.interpreter.nabl2.f.Addr;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.building.TermBuild;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.matching.MatchPattern;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.rules.PremiseFailureException;
import org.spoofax.interpreter.terms.IStrategoTerm;

import com.oracle.truffle.api.dsl.Fallback;
import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.NodeChildren;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

public final class ArrayAddr extends Addr {

	private final Array arr;
	private final int idx;

	public ArrayAddr(Array arr, int idx) {
		this.arr = arr;
		this.idx = idx;
	}

	public Array arr() {
		return arr;
	}

	public int idx() {
		return idx;
	}

	@Override
	public int size() {
		return 2;
	}

	public Array get_1() {
		return arr();
	}

	public int get_2() {
		return idx();
	}

	@Override
	public boolean hasStrategoTerm() {
		return false;
	}

	@Override
	public IStrategoTerm getStrategoTerm() {
		return null;
	}

	@NodeChildren({ @NodeChild(value = "arr", type = TermBuild.class),
			@NodeChild(value = "idx", type = TermBuild.class) })
	public abstract static class Build extends TermBuild {

		public Build(SourceSection source) {
			super(source);
		}

		@Specialization // (replaces = "buildCached")
		public ArrayAddr buildUncached(Array arr, int idx) {
			return new ArrayAddr(arr, idx);
		}

	}

	public abstract static class Match extends MatchPattern {

		@Child private MatchPattern arr_pat;
		@Child private MatchPattern key_pat;

		public Match(SourceSection s, MatchPattern frame_pat, MatchPattern arr_pat) {
			super(s);
			this.arr_pat = frame_pat;
			this.key_pat = arr_pat;
		}

		@Specialization
		public void doDeepMatch(VirtualFrame frame, ArrayAddr addr) {
			arr_pat.executeMatch(frame, addr.arr);
			key_pat.executeMatch(frame, addr.idx);
		}

		@Fallback
		public void doGeneric(VirtualFrame frame, Object term) {
			if (term instanceof ArrayAddr) {
				doDeepMatch(frame, (ArrayAddr) term);
			} else {
				throw PremiseFailureException.SINGLETON;
			}
		}

	}

}
