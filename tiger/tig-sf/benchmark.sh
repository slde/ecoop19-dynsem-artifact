#! /bin/bash

set -e
set -x

ROOTDIR="$(pwd)/../../"
pushd $ROOTDIR
source env.sh
popd

export JAVA_HOME="$GVM_HOME"

TIGER_LANG="$(pwd)/org.metaborg.lang.tiger"
TIGER_INTERP="$(pwd)/lang.tiger.interp.scopesandframes"

TIGER_JAR="$TIGER_INTERP/target/lang.tiger.interp.scopesandframes-0.0.1-SNAPSHOT.jar"

# construct class path using maven
pushd $TIGER_INTERP
mvn dependency:build-classpath -s $MAVEN_SETTINGS -Dmdep.outputFile=cp.txt
popd

BOOTCLASSPATH="-Xbootclasspath/a:\
$JAVA_HOME/jre/lib/truffle/truffle-api.jar:\
$JAVA_HOME/jre/lib/truffle/locator.jar:\
$JAVA_HOME/jre/lib/truffle/truffle-nfi.jar"

export SPOOFAXPATH=${TIGER_LANG}

function run_workload_bkg {
  WORKLOAD=$1
  RESULTS_FILE=$2
  nice -n -5 $JAVA_HOME/bin/java $BOOTCLASSPATH -Xss64m \
  -Dgraal.TruffleCompilationExceptionsAreFatal=true \
-Dgraal.TruffleCompilationThreshold=200 \
-Dgraal.TruffleInliningMaxCallerSize=10000 \
-Dgraal.TruffleSplittingMaxCalleeSize=100000 \
-Dgraal.TruffleBackgroundCompilation=true \
-Dgraal.GraalCompileOnly='org.metaborg.meta.lang.dynsem.interpreter.*.*,org.metaborg.lang.tiger.interpreter.*.*,org.metaborg.lang.tiger.interp.scopesandframes.*.*' \
  -cp $TIGER_JAR:$(cat $TIGER_INTERP/cp.txt) \
  org.metaborg.lang.tiger.interp.scopesandframes.TigerInterpreter $WORKLOAD > $RESULTS_FILE 2>&1 \
  || echo ">>>> RUN CRASHED <<<<"
}

VARIANT="tig-sf"
RESULTSDIR="$ROOTDIR/results/$VARIANT/"
BENCHDIR="$ROOTDIR/benchmarks/tiger/"
mkdir -p $RESULTSDIR

for i in {0..29}
do
  # list
  run_workload_bkg $BENCHDIR/list.tig $RESULTSDIR/list_bkg_$i.out
  # noop
  run_workload_bkg $BENCHDIR/noop.tig $RESULTSDIR/noop_bkg_$i.out
  # permute
  run_workload_bkg $BENCHDIR/permute.tig $RESULTSDIR/permute_bkg_$i.out
  # queens2
  run_workload_bkg $BENCHDIR/queens2.tig $RESULTSDIR/queens2_bkg_$i.out
  # sieve
  run_workload_bkg $BENCHDIR/sieve.tig $RESULTSDIR/sieve_bkg_$i.out
  # towers
  run_workload_bkg $BENCHDIR/towers.tig $RESULTSDIR/towers_bkg_$i.out
  # bubblesort
  run_workload_bkg $BENCHDIR/bubblesort.tig $RESULTSDIR/bubblesort_bkg_$i.out
done
