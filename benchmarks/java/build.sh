#! /bin/bash

set -e
set -x

ROOTDIR="$(pwd)/../../"
pushd $ROOTDIR
source env.sh
popd

export JAVA_HOME="$GVM_HOME"

javac *.java
