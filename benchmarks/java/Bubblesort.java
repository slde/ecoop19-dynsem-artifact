public final class Bubblesort {

	private static final class Cons {
		public int val;
		public Cons next;

		Cons(int val, Cons next) {
			this.val = val;
			this.next = next;
		}

	}
	
	public void bubblesort(Cons ll) {
		boolean cont = true;
		boolean swapped = false;
		while(cont) {
			swapped = false;
			Cons current = ll;
			while(current.next != null) {
				if(current.val > current.next.val) {
					int tmp = current.val;
					current.val = current.next.val;
					current.next.val = tmp;
					swapped = true;
				}
				current = current.next;
			}
			cont = swapped;
		}
	}

	public Object benchmark() {
		Cons ll = null;
		for(int i = 0; i < 500; i++) {
			ll = new Cons(i, ll);
		};
		bubblesort(ll);
		return ll;
	}

	
	public static void main(String[] args) {
		int numruns = 200;
		long[] runtimes = new long[numruns];
		for (int i = 0; i < numruns; i++) {
			long st = System.nanoTime();
			new Bubblesort().benchmark();
			long et = System.nanoTime();
			runtimes[i] = et - st;
		}
		writeTimes(runtimes);
	}

	private final static void writeTimes(long[] runtimes) {
		StringBuilder builder = new StringBuilder();
		builder.append(">>>>>RUNTIMES>>>>\n");
		for (long runtime : runtimes) {
			builder.append(runtime);
			builder.append("\n");
		}
		builder.append("<<<<<RUNTIMES<<<<\n");
		System.out.println(builder);
	}
}
