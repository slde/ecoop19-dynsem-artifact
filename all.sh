#! /bin/bash

set -e
set -x

sudo echo "caching sudo rights"

pushd benchmarks/java
./build.sh && sudo ./benchmark.sh
popd

pushd tiger/meta-env-noinlining
./build.sh && sudo ./benchmark.sh
popd

pushd tiger/meta-sf-inlining
./build.sh && sudo ./benchmark.sh
popd

pushd tiger/meta-sf-noinlining
./build.sh && sudo ./benchmark.sh
popd

pushd tiger/tig-sf
./build.sh && ./benchmark.sh
popd
