module dynamics/frames-native/functions


imports
  dynamics/frames-native/nabl2/nabl2-link
  dynamics/frames-native/base
  dynamics/frames-native/bindings
  
signature
  arrows
    Frames1 |- Map2-FunDecs(List(FunDec)) --> U
    Frames1 |- FunDec --> U
    unpackArgs(List(FArg)) --> List(Occurrence)
    Frames1 |- Zip-Params(List(Exp), List(Occurrence)) --> U
    F |- evals(List(Exp)) --> List(V)
    F |- binds(List(V), List(Occurrence)) --> U
        
  constructors
    FunV: Frame * Scope * List(Occurrence) * Exp -> V

rules
  
  default(FUN(_, _)) --> UndefV()

rules // function declarations

  Frames1 (F, F_outer) |- Dec(FunDecs(fdecs)) --> U()
  where
    Frames1 (F, F_outer) |- Map2-FunDecs(fdecs) --> _
  
  Frames1 (F, F') |- Map2-FunDecs([]) --> U()
  
  Frames1 (F, F') |- Map2-FunDecs([x|xs]) --> U()
  where
    Frames1 (F, F') |- x --> _;
    Frames1 (F, F') |- Map2-FunDecs(xs) --> _  
  
  Frames1 (F, F_outer) |- d@ProcDec(f : Occurrence, args, e) --> U()
  where
    set(F, f, FunV(F, scopeOfTerm(d), unpackArgs(args), e)) => _
  
  Frames1 (F, F_outer) |- d@FunDec(f : Occurrence, args, t, e) --> U()
  where
    set(F, f, FunV(F, scopeOfTerm(d), unpackArgs(args), e)) => _
  
  F |- Call(f : Occurrence, exps) --> v
  where
    get(lookup(F, f)) => FunV(F_parent, s_fun, fargs, e);
    link(newframe(s_fun), L(P(), F_parent)) => F_call;
    F |- evals(exps) --> aargs;
    F_call |- binds(aargs, fargs) --> _;
    F_call |- e --> v
    
  unpackArgs([]) --> []
  
  unpackArgs([FArg(x : Occurrence, _) | args]) --> [x |occs]
  where
    unpackArgs(args) --> occs

  evals([]) --> []
  
  evals([e|es]) --> [v|vs]
  where
    e --> v;
    evals(es) --> vs

  binds([], []) --> U()

  F |- binds([v|vs], [p|ps]) --> binds(vs,ps)
  where
    set(F, p, v) => _

//  Zip-Params([], []) --> U()
//  
//  Frames1 (F, F_call) |- Zip-Params([e|es], [p|ps]) --> Zip-Params(es, ps)
//  where
//    F |- e --> v;
//    set(F_call, p, v) => _

