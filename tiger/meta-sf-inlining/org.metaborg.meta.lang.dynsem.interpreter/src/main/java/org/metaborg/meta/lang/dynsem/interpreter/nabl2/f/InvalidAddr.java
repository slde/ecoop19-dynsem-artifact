package org.metaborg.meta.lang.dynsem.interpreter.nabl2.f;

import org.metaborg.meta.lang.dynsem.interpreter.nodes.building.TermBuild;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.matching.MatchPattern;
import org.metaborg.meta.lang.dynsem.interpreter.nodes.rules.PremiseFailureException;
import org.spoofax.interpreter.terms.IStrategoTerm;

import com.oracle.truffle.api.CompilerDirectives.ValueType;
import com.oracle.truffle.api.dsl.Fallback;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

@ValueType
public final class InvalidAddr extends Addr {

	public static final InvalidAddr SINGLETON = new InvalidAddr();

	public InvalidAddr() {
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public boolean hasStrategoTerm() {
		return false;
	}

	@Override
	public IStrategoTerm getStrategoTerm() {
		return null;
	}

	@Override
	public String toString() {
		return "InvalidAddr()";
	}

	public abstract static class Build extends TermBuild {

		public Build(SourceSection source) {
			super(source);
		}

		@Specialization // (replaces = "buildCached")
		public InvalidAddr buildUncached() {
			return new InvalidAddr();
		}

	}

	public abstract static class Match extends MatchPattern {

		public Match(SourceSection s) {
			super(s);
		}

		@Specialization
		public void doDeepMatch(VirtualFrame frame, InvalidAddr addr) {

		}

		@Fallback
		public void doGeneric(VirtualFrame frame, Object term) {
			if (term instanceof InvalidAddr) {
				doDeepMatch(frame, (InvalidAddr) term);
			} else {
				throw PremiseFailureException.SINGLETON;
			}
		}

	}

}
