#! /bin/bash

set -e
set -x

ROOTDIR="$(pwd)/../../"
pushd $ROOTDIR
source env.sh
popd

pushd dynsem
mvn -s $MAVEN_SETTINGS clean install
popd

pushd org.metaborg.meta.lang.dynsem.interpreter
mvn -s $MAVEN_SETTINGS clean install
popd

pushd org.metaborg.lang.tiger.interpreter
mvn -s $MAVEN_SETTINGS clean
popd
pushd org.metaborg.lang.tiger
mvn -s $MAVEN_SETTINGS clean verify
popd
pushd org.metaborg.lang.tiger.interpreter
mvn -s $MAVEN_SETTINGS verify
popd
